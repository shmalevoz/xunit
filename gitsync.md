# Робот выгрузки хранилища 1С 8.3 на Git

Инструкция по установке для Ubuntu server 22.04

## Системное ПО

Необходимо установить OneScript

[OneScript](https://oscript.io) ([страница загрузок](https://oscript.io/downloads)), пример будет на версии 1.7.0

```bash
wget -O ~/oscript.deb https://oscript.io/downloads/latest/x64/onescript-engine_1.7.0_all.deb
sudo dpkg -i ~/oscript.deb
sudo apt --fix-broken install -y
sudo opm install gitsync
```

проверяем корректность установки

```bash
opm ls
```

должен быть выведен список установленных модулей с присутствующим *gitsync*

## Создание скрипта синхронизации и/или первичная выгрузка

Используется скрипт [makesync](https://gitlab.com/shmalevoz/scripts/-/blob/master/makesync/makesync) ([скачать](https://gitlab.com/shmalevoz/scripts/-/raw/master/makesync/makesync?inline=false)) проекта scripts

Полученный скрипт синхронизации повесить на cron